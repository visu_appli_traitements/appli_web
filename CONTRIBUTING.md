# Contributing

This short guide will help you in your quest of information on about how to contribute to this project. In order to have to patch accepted, please follow strictly the following rules. This ensures the code’s quality and regularity regarding syntax and standards we use.

## Table of contents

* [Development](#development)
* [Coding style](#coding-style)
 * [PHP style](#php-coding-style)
 * [Javascript](#javascript-coding-style)
* [Commit message](#commit-message)
* [Workflow](#workflow)

### Development

During your development session, you need to execute two tasks :

* Transform SASS files into CSS (style.scss => style.css)
* Display ESLint errors and warnings in your console

To help you and automate it, we have deploy [Gulp](https://gulpjs.com/). You simply have to run from project's root :

```bash
gulp
```

### Coding style

#### PHP Coding style

As we develop using the PHP langage, we follow the [PEAR](https://pear.php.net/manual/en/standards.php) and [PSR-2](https://pear.php.net/manual/en/standards.php) coding standards. We use the project [PHP_CodeSniffer](https://github.com/PHPCheckstyle/phpcheckstyle) to ensure the code follows the specification. A continuous integration procedure automatically checks that respect to the standard syntax is correct. An `.editorconfig` file is included in the repository to ensure the syntax is as close as possible to the PEAR coding standard.

We use `phpcs` and `phpcbf` to ensure the standards are respected. A runner is configured on the server to automatically detect any coding style problem.

An [installation procedure using **Composer**](https://github.com/squizlabs/PHP_CodeSniffer) is given on their repository, or, on **Fedora > 28**, you can install it using the following command:

```bash
dnf install -y php-{cli,json,zip,xml,pear-PHP-CodeSniffer}
```

NOTE: A pipeline checks automatically errors on develop and merge requests. We do not take warnings into account for development steps. But, if a merge request is made on **master**, you must fix all the warnings given in the code before proceeding.

##### Autocheck

We use the following command to check for style issues:

```bash
phpcs --standard=ruleset.xml -s -n visualize_treatments
```

The `-n` option is used to remove **warnings** from the report.

##### Autocorrection for errors

`phpcbf` can be used to automatically fix errors found in the code:

```bash
phpcbf --standard=ruleset.xml -n visualize_treatments
```

#### Javascript coding style

The components that are written in Javascript follow the [AirBNB coding standard](https://github.com/airbnb/javascript).  We use [ESLint](https://eslint.org) to ensure the code follows the specification. A continuous integration procedure automatically checks that respect to the standard syntax is correct. An `.eslintrc.json` file is included in the repository to configure ESLint.

ESLint can be installed using NPM (throught Node), as an example, on **Fedora > 28**, you may use:
```bash
dnf install nodejs
```

and then:
```bash
npm install -g eslint && npm install -g eslint-plugin-import && npm install -g eslint-config-airbnb-base
```

NOTE: A pipeline checks automatically errors on develop, master and merge requests. We do not distinguish errors and warnings. You will always have to fix what’s given by ESlint.

##### Autocheck and fixing errors
You can check errors using ESling from the command line:
```bash
eslint assets/js/*.js
```

You can add `--fix` option to automatically fix errors.

### Commit message

The commit message follows the template by Deekshith Allamaneni (*aka* [adeekshith](https://gist.github.com/adeekshith)) which can be found at : [https://gist.github.com/adeekshith/cd4c95a064977cdc6c50](https://gist.github.com/adeekshith/cd4c95a064977cdc6c50). 

```textile
# <type>: (If applied, this commit will...) <subject> (Max 50 char)
# |<----  Using a Maximum Of 50 Characters  ---->|


# Explain why this change is being made
# |<----   Try To Limit Each Line to a Maximum Of 72 Characters   ---->|

# Provide links or keys to any relevant tickets, articles or other resources
# Example: Github issue #23

# --- COMMIT END ---
# Type can be 
#    feat     (new feature)
#    fix      (bug fix)
#    refactor (refactoring production code)
#    style    (formatting, missing semi colons, etc; no code change)
#    docs     (changes to documentation)
#    test     (adding or refactoring tests; no production code change)
#    chore    (updating grunt tasks etc; no production code change)
# --------------------
# Remember to
#    Capitalize the subject line
#    Use the imperative mood in the subject line
#    Do not end the subject line with a period
#    Separate subject from body with a blank line
#    Use the body to explain what and why vs. how
#    Can use multiple lines with "-" for bullet points in body
# --------------------
# For more information about this template, check out
# https://gist.github.com/adeekshith/cd4c95a064977cdc6c50
```

You can configure this template in your local project by downloading it and calling `git config`.

```bash
curl -s https://gist.githubusercontent.com/adeekshith/cd4c95a064977cdc6c50/raw/4c9c61000f15f1e8927628bcd2e506c45bf1abc2/.git-commit-template.txt -o ~/.git-commit-template.txt
git config commit.template ~/.git-commit-template.txt
```

This way, each time you call `git commit` **without** the `--message` option, the template will appear to help you write the commit message.

### Workflow

The entire process is based on Gitlab Issues. If you notice a bug, want an enchancement or just give us some news, you have to open a new issue. We use issues to follow the implementation procedure.

#### Procedure

1. Open an issue using the appropriate templates : Bug, Enchancement, etc.

2. The issue will be assigned to a specific milestone to be treated as soon as possible if required.

3. Once somebody is assigned to the issue, he’ll be invited to create a new branch **from develop** using Gitlab. He’ll work on the feature or the bug **on this branch only**. Commit message must **mention the issue number using the  Gitlab syntax**: if you’d like to quote a specific issue by its number, call it using `#1` for instance. It’s `!1` for merge requests.

4. The issue will move into the board from the “To Do“ state to “Done“.

5. Once done, a new merge request from this branch is submitted to include the modifications into develop.

6. Once done, if the tests passed and there are no merge errors, a maintener will accept, or refuse the proposed modificaition and merge it into **develop**.
