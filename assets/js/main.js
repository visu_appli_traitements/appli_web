/**
 * Copyright:
 * Guillaume Bernard <contact@guillaume-bernard.fr>
 * Thomas Duveau <tduveau@etudiant.univ-lr.fr>
 * Loïc Favrelière <lfavreli@etudiant.univ-lr.fr>
 * Nicola Foissac <nfoissac@etudiant.univ-lr.fr>
 *
 * This software is a computer program whose purpose is to visualize in
 * a simple way the record of procesing activites as defined in the GDPR.
 *
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 **/

/* JavaScript file for handling client-side views */
/* global $, d3, c3, jsonDataset */

$(document).ready(function () {
  // Initialize pie chart on appropriate page
  if ($('#pieChart').length === 1 && jsonDataset) initClassicView();
});

function initClassicView () {
  // /////////////////////////////
  // ///////// GLOBAL ////////////
  // /////////////////////////////

  var checkboxesCategoriesPath = 'input[name="sidebarClassic"]';
  var checkedCategoriesPath = 'input[name="sidebarClassic"]:checked';

  function updateCriterias (selectedCheckbox) {
    // First of all, update client-side view
    handleCriteriasStyles(selectedCheckbox);

    // Toogle checkboxes value
    selectedCheckbox.prop('checked', !selectedCheckbox.prop('checked'));

    // Get all checked categories
    var categories = $(checkedCategoriesPath).map(function () {
      return $(this).val();
    }).toArray();

    var dataset = (categories.length > 0)
      ? getTreatmentsByCategories(jsonDataset, categories)
      : jsonDataset;

    drawDatatables(dataset); // Redraw Datatables
  }

  function resetCriterias () {
    // Reset default style on sidebar (highlighting).
    $('.sidebar-custom-checkbox.highlighted').removeClass('highlighted');

    // Unchecked all previous checkboxes.
    $(checkboxesCategoriesPath).prop('checked', false);

    // Remove the defocused css class
    $('.c3-arc').removeClass('defocused');

    // Redraw Datatables with defaults values.
    drawDatatables(jsonDataset);
  }

  function handleCriteriasStyles (selectedCheckbox) {
    // If there is no previous selected category, defocus all of them
    if ($(checkedCategoriesPath).length === 0) {
      $('.c3-arc').addClass('defocused');
    }

    // Toggle focus/opacity region in pie chart
    $('.c3-arc-' + selectedCheckbox.val()).toggleClass('defocused');

    // Remove defocused css class when all categories are unchecked
    if ($(checkboxesCategoriesPath).length === $('.defocused').length) {
      $('.c3-arc').removeClass('defocused');
    }

    // Toggle highlighted class for sidebar
    $(selectedCheckbox).parent('li').toggleClass('highlighted');
  }

  // //////////////////////////////////////////////////////
  // //////////////// Handle Datatables ///////////////////
  // //////////////////////////////////////////////////////

  // Function who display properly categories on column
  function renderCategories (data, type, row) {
    if (data) {
      var label = '<b>Primaire</b> : ' + data.primaire.join(', ') + '<br/>';

      if (data.secondaire.length > 0) {
        label += '\n<b>Secondaire</b> : ' + data.secondaire.join(', ') + '<br/>';
      }

      if (data.tertiaire.length > 0) {
        label += '\n<b>Tertiaire</b> : ' + data.tertiaire.join(', ') + '<br/>';
      }
      return label;
    }
    return 'Aucune catégorie';
  }

  // Get columns according selected source
  function getColumns (source) {
    var firstCol = (source === 'destinataire')
      ? 'fields.destinataires_des_donnees_autres_que_service_en_charge_de_la_mise_en_oeuvre'
      : 'fields.service_en_charge_de_la_mise_en_oeuvre';

    var secondCol = (source === 'destinataire')
      ? 'categories.destinataire'
      : 'categories.source_mise_oeuvres';

    return [
      { data: firstCol },
      { data: secondCol },
      { data: 'fields.finalite_s' },
      { data: 'fields.duree_de_conservation' },
      { data: 'fields.contact_pour_exercice_des_droits_des_personnes' }
    ];
  }

  // Initialize Datatables (data, columns, options...)
  function initDatatables (source) {
    return $('#treatments_details').DataTable({
      data: jsonDataset,
      scrollY: 400,
      columns: getColumns(source),
      columnDefs: [ { render: renderCategories, targets: 1 } ],
      language: {
        processing: 'Traitement en cours...',
        search: 'Rechercher&nbsp;:',
        lengthMenu: 'Afficher _MENU_ &eacute;l&eacute;ments',
        info: 'Affichage de l\'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments',
        infoEmpty: 'Affichage de l\'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments',
        infoFiltered: '(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)',
        infoPostFix: '',
        loadingRecords: 'Chargement en cours...',
        zeroRecords: 'Aucun &eacute;l&eacute;ment &agrave; afficher',
        emptyTable: 'Aucune donnée disponible dans le tableau',
        paginate: {
          first: 'Premier',
          previous: 'Pr&eacute;c&eacute;dent',
          next: 'Suivant',
          last: 'Dernier'
        },
        aria: {
          sortAscending: ': activer pour trier la colonne par ordre croissant',
          sortDescending: ': activer pour trier la colonne par ordre décroissant'
        }
      }
    });
  }

  // Draw or redraw Datatables on page when it needed
  function drawDatatables (data) {
    table.clear() // Clear the Datatable
      .rows.add(data) // Add choosen treatments
      .columns.adjust().draw(); // Adjust column and redraw
  }

  // Get appropriate treatments according selected categories
  function getTreatmentsByCategories (treatments, categories) {
    return treatments.filter(function (treatment) {
      if (treatment.categories !== undefined) {
        // Get checked source
        var source = $('input[name="source"]:checked').val();

        var catPrimary = (source === 'destinataire')
          ? treatment.categories.destinataire.primaire
          : treatment.categories.source_mise_oeuvres.primaire;

        // Compute intersection between treatments and selected categories
        var intersection = catPrimary.filter(function (category) {
          return categories.indexOf(category) !== -1;
        });

        // If intersection is not empty, keep treatment
        if (intersection.length > 0) return true;
        else return false;
      }
      return false;
    });
  }

  // Initialize Datatables when document is ready
  var table = initDatatables('destinataire');

  // ////////////////////////////////
  // ///////// Pie Chart ////////////
  // ////////////////////////////////

  function getValuesFromSource (source) {
    // Count all primary categories with a map/reduce
    var counts = $.map(datasetCategories, function (treatment) {
      return (source === 'destinataire')
        ? treatment.categories.destinataire.primaire
        : treatment.categories.source_mise_oeuvres.primaire;
    }).reduce(function (acc, curr) {
      if (acc[curr]) acc[curr]++;
      else acc[curr] = 1;
      return acc;
    }, {});

    // Format results for Pie Chart
    return $.map(counts, function (count, category) {
      return [ [category, count] ];
    });
  }

  // TEMP: Ignore Data Without Category
  var datasetCategories = jsonDataset.filter(function (obj) {
    return obj.categories !== undefined;
  });

  // Draw Pie Chart on screen
  var chart = c3.generate({
    bindto: '#pieChart',
    data: {
      columns: getValuesFromSource('destinataire'),
      type: 'pie',
      color: function (color, data) {
        return d3.rgb(color).brighter(0.3);
      },
      onmouseover: function () {
        $('.c3-chart-arc').removeClass('c3-defocused');
      },
      onclick: function (data, el) {
        var checkboxContext = $('.sidebar-custom-checkbox input' + '[value="' + data.name + '"]');
        updateCriterias(checkboxContext);
      }
    }
  });

  // Resize chart to be responsive when page dimensions are updated
  $(window).on('resize', function () {
    chart.resize();
  });

  // //////////////////////////////
  // ///////// Sidebar ////////////
  // //////////////////////////////

  // Reset styles and checkbox when page is refreshed
  resetCriterias();

  // Display tooltip when mouse hover
  $('[data-toggle="tooltip"]').tooltip({ trigger: 'hover' });

  // Handle selected source
  $('.source').click(function () {
    // Get source value
    var source = $(this).children('input').val();

    // Destroy old Datatables and initialize a new one
    table.destroy();
    table = initDatatables(source);

    var header = (source === 'destinataire') ? 'Services destinataire' : ' Services de mise en oeuvre';
    $('#source').text(header);

    chart.load({ unload: true, columns: getValuesFromSource(source) });

    // When source is updated, reset criterias
    resetCriterias();
  });

  // Unselects categories when user clicked on reset button
  $('#reset').click(resetCriterias);

  // Select a category on sidebar
  $('li.sidebar-custom-checkbox').click(function () {
    // Get checkbox context and update criterias on sidebar and Datatables
    var selectedCheckbox = $(this).children('.form-check-input');
    updateCriterias(selectedCheckbox);
  });
}
