/**
 * Copyright:
 * Guillaume Bernard <contact@guillaume-bernard.fr>
 * Thomas Duveau <tduveau@etudiant.univ-lr.fr>
 * Loïc Favrelière <lfavreli@etudiant.univ-lr.fr>
 * Nicola Foissac <nfoissac@etudiant.univ-lr.fr>
 *
 * This software is a computer program whose purpose is to visualize in
 * a simple way the record of procesing activites as defined in the GDPR.
 *
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 *
 * This code is based on the work, under MIT Licence, of Nadieh Bremer freelancer in Data Visualisation,
 * designer and artist.
 * Repository : https://github.com/nbremer/occupationscanvas
 * Version of November 2015
 **/

/* global $, d3version3, queue, navigator */
$(document).ready(function () {
  if ($('#chart').length === 1) {
    queue().defer(d3version3.csv, '/assets/data/sous_categories.csv')
      .defer(d3version3.csv, '/assets/data/data.csv')
      .defer(d3version3.json, '/assets/data/data_format.json')
      .await(drawAll);
  }

  // Initiates practically everything
  function drawAll (error, catCSV, idCSV, categories) {
    // Handle possible error
    if (error) return;

    // ////////////////////////////////////////////////////////////
    // //////////////// Create Set-up variables  //////////////////
    // ////////////////////////////////////////////////////////////

    // Trying to figure out how to detect touch devices (exept for laptops with touch screens)
    // Since there's no need to have a mouseover function for touch
    // There has to be a more foolproof way than this...
    window.mobileAndTabletcheck = function () {
      var check = false;
      (function (a) {
        if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw-(n|u)|c55\/|capi|ccwa|cdm-|cell|chtm|cldc|cmd-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc-s|devi|dica|dmob|do(c|p)o|ds(12|-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(-|_)|g1 u|g560|gene|gf-5|g-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd-(m|p|t)|hei-|hi(pt|ta)|hp( i|ip)|hs-c|ht(c(-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i-(20|go|ma)|i230|iac( |-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|-[a-w])|libw|lynx|m1-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|-([1-8]|c))|phil|pire|pl(ay|uc)|pn-2|po(ck|rt|se)|prox|psio|pt-g|qa-a|qc(07|12|21|32|60|-[2-7]|i-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h-|oo|p-)|sdk\/|se(c(-|0|1)|47|mc|nd|ri)|sgh-|shar|sie(-|m)|sk-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h-|v-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl-|tdg-|tel(i|m)|tim-|t-mo|to(pl|sh)|ts(70|m-|m3|m5)|tx-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas-|your|zeto|zte-/i.test(a.substr(0, 4))) check = true;
      })(navigator.userAgent || navigator.vendor || window.opera);
      return check;
    }; // function mobileAndTabletcheck
    var mobileSize = window.mobileAndTabletcheck();

    var padding = 410;
    var width = Math.max($('#chart').innerWidth(), 350) - padding;
    var height = (mobileSize | window.innerWidth < 768 ? width : window.innerHeight - 250);

    var centerX = width / 2;
    var centerY = height / 2;

    // /////////////////////////////////////////////////////////
    // ///////////////////// Create SVG  ///////////////////////
    // /////////////////////////////////////////////////////////

    // Create the visible canvas and context
    var canvas = d3version3.select('#chart').append('canvas')
      .attr('id', 'canvas')
      .attr('width', width)
      .attr('height', height);

    var context = canvas.node().getContext('2d');
    context.clearRect(0, 0, width, height);

    // Create a hidden canvas in which each circle will have a different color
    // We can use this to capture the clicked/hovered over on circle
    var hiddenCanvas = d3version3.select('#chart').append('canvas')
      .attr('id', 'hiddenCanvas')
      .attr('width', width)
      .attr('height', height)
      .style('display', 'none');

    var hiddenContext = hiddenCanvas.node().getContext('2d');
    hiddenContext.clearRect(0, 0, width, height);

    /// ///////////////////////////////////////////////////////////
    /// //////////////////// Create Scales  ///////////////////////
    /// ///////////////////////////////////////////////////////////

    var mainTextColor = [74, 74, 74];
    var titleFont = 'Oswald';

    var colorCircle = d3version3.scale.ordinal()
      .domain([0, 1, 2, 3])
      .range(['#e3f2fd', '#0094ff', '#4c4c4c', '#1c1c1c']);

    var diameter = Math.min(width * 0.9, height * 0.9);

    var zoomInfo = {
      centerX: centerX,
      centerY: centerY,
      scale: 1
    };

    // Dataset to swtich between color of a circle (in the hidden canvas) and the node data
    var colToCircle = {};

    var pack = d3version3.layout.pack()
      .padding(1)
      .size([diameter, diameter])
      .value(function (d) {
        return d.size;
      })
      .sort(function (d) {
        return d.ID;
      });

    /// ///////////////////////////////////////////////////////////
    /// /////////// Create Circle Packing Data ////////////////////
    /// ///////////////////////////////////////////////////////////

    var nodes = pack.nodes(categories);
    var root = categories;
    var focus = root;
    var nodeCount = nodes.length;

    var nodeByName = {};
    nodes.forEach(function (d, i) {
      nodeByName[d.name] = d;
    });

    /// ///////////////////////////////////////////////////////////
    /// ////////////// Create Bar Chart Data //////////////////////
    /// ///////////////////////////////////////////////////////////

    // Turn the value into an actual numeric value
    catCSV.forEach(function (d) { d.value = +d.value; });

    // Create new dataset grouped by ID
    var data = d3version3.nest()
      .key(function (d) { return d.ID; })
      .entries(catCSV);

    // Array to keep track of which ID belongs to which index in the array
    var dataById = {};
    data.forEach(function (d, i) {
      dataById[d.key] = i;
    });

    var IDbyName = {};
    // Small file to get the IDs of the non leaf circles
    idCSV.forEach(function (d, i) {
      IDbyName[d.name] = d.ID;
    });

    /// ///////////////////////////////////////////////////////////
    /// ////////////// Canvas draw function ///////////////////////
    /// ///////////////////////////////////////////////////////////

    // The draw function of the canvas that gets called on each frame
    function drawCanvas (chosenContext, hidden) {
      // Clear canvas
      chosenContext.fillStyle = '#fff';
      chosenContext.rect(0, 0, width, height);
      chosenContext.fill();

      // Select our dummy nodes and draw the data to canvas.
      var node = null;

      // It's slightly faster than nodes.forEach()
      for (var i = 0; i < nodeCount; i++) {
        node = nodes[i];

        // If the hidden canvas was send into this function and it does not yet have a color, generate a unique one
        if (hidden) {
          if (node.color == null) {
            // If we have never drawn the node to the hidden canvas get a new color for it and put it in the dictionary.
            node.color = genColor();
            colToCircle[node.color] = node;
          }// if
          // On the hidden canvas each rectangle gets a unique color.
          chosenContext.fillStyle = node.color;
        } else {
          chosenContext.fillStyle = node.children ? colorCircle(node.depth) : '#ebebff';
        }// else

        var nodeX = ((node.x - zoomInfo.centerX) * zoomInfo.scale) + centerX;
        var nodeY = ((node.y - zoomInfo.centerY) * zoomInfo.scale) + centerY;
        var nodeR = node.r * zoomInfo.scale;

        // Draw each circle
        chosenContext.beginPath();
        chosenContext.arc(nodeX, nodeY, nodeR, 0, 2 * Math.PI, true);
        chosenContext.fill();

        // Draw the bars inside the circles (only in the visible canvas)
        // Only draw bars in leaf nodes
        if (node.ID in dataById) {
          // Only draw the bars that are in the same parent ID as the clicked on node
          if (node.ID.lastIndexOf(currentID, 0) === 0 & !hidden) {
            // Variables for the bar title
            var drawTitle = true;
            var fontSizeTitle = Math.round(nodeR / 10);
            if (fontSizeTitle < 8) drawTitle = false;

            // Only draw the title if the font size is big enough
            if (drawTitle & showText) {
              // Get the text back in pieces that will fit inside the node
              var titleText = getLines(chosenContext, node.name, nodeR * 2 * 0.7, fontSizeTitle, titleFont);
              // Loop over all the pieces and draw each line
              titleText.forEach(function (txt, iterator) {
                chosenContext.font = fontSizeTitle + 'px ' + titleFont;
                chosenContext.fillStyle = 'rgba(' + mainTextColor[0] + ',' + mainTextColor[1] + ',' + mainTextColor[2] + ',' + textAlpha + ')';
                chosenContext.textAlign = 'center';
                chosenContext.textBaseline = 'middle';
                chosenContext.fillText(txt, nodeX, nodeY + (-0.65 + iterator * 0.125) * nodeR);
              });// forEach
            }// if
          } // if -> node.ID.lastIndexOf(currentID, 0) === 0 & !hidden
        }// if -> node.ID in dataById
      }// for i

      var counter = 0; // Needed for the rotation of the arc titles

      // Do a second loop because the arc titles always have to be drawn on top
      for (var index = 0; index < nodeCount; index++) {
        node = nodes[index];

        nodeX = ((node.x - zoomInfo.centerX) * zoomInfo.scale) + centerX;
        nodeY = ((node.y - zoomInfo.centerY) * zoomInfo.scale) + centerY;
        nodeR = node.r * zoomInfo.scale;

        // Don't draw for leaf-nodes
        // And don't draw the arced label for the largest outer circle
        // And don't draw these things for the hidden layer
        // And only draw these while showText = true (so not during a zoom)
        // And hide those not close the the parent
        if (typeof node.parent !== 'undefined' & typeof node.children !== 'undefined') {
          if (node.name !== 'categorie' & !hidden & showText & $.inArray(node.name, kids) >= 0) {
            // Calculate the best font size for the non-leaf nodes
            fontSizeTitle = Math.round(14);
            if (fontSizeTitle > 4) drawCircularText(chosenContext, node.name.replace(/,? and /g, ' & '), fontSizeTitle, titleFont, nodeX, nodeY, nodeR, rotationText[counter], 0);
          }// if
          counter = counter + 1;
        }// if
      }// for i
    }// function drawCanvas

    /// ///////////////////////////////////////////////////////////
    /// //////////////// Click functionality //////////////////////
    /// ///////////////////////////////////////////////////////////

    // Default values for variables - set to root
    var currentID = '';
    var kids = ['categorie']; // needed to check which arced titles to show - only those close to the parent node

    // Setup the kids variable for the top (root) level
    for (var i = 0; i < root.children.length; i++) {
      kids.push(root.children[i].name);
    }

    // Function to run oif a user clicks on the canvas
    var clickFunction = function (e) {
      // Figure out where the mouse click occurred.
      var mouseX = e.offsetX; // e.layerX;
      var mouseY = e.offsetY; // e.layerY;

      // Get the corresponding pixel color on the hidden canvas and look up the node in our map.
      // This will return that pixel's color
      var col = hiddenContext.getImageData(mouseX, mouseY, 1, 1).data;
      // Our map uses these rgb strings as keys to nodes.
      var colString = 'rgb(' + col[0] + ',' + col[1] + ',' + col[2] + ')';
      var node = colToCircle[colString];

      // If there was an actual node clicked on, zoom into this
      if (node) {
        // If the same node is clicked twice, set it to the top (root) level
        if (focus === node) node = root;

        // Save the names of the circle itself and first children
        // Needed to check which arc titles to show
        kids = [node.name];
        if (typeof node.children !== 'undefined') {
          for (var i = 0; i < node.children.length; i++) {
            kids.push(node.children[i].name);
          }// for i
        }// if

        // Perform the zoom
        zoomToCanvas(node);
      }// if -> node
    };// function clickFunction

    // Listen for clicks on the main canvas
    $('#canvas').on('click', clickFunction);

    /// ///////////////////////////////////////////////////////////
    /// ///////////// Mousemove functionality /////////////////////
    /// ///////////////////////////////////////////////////////////

    // Only run this if the user actually has a mouse
    if (!mobileSize) {
      var nodeOld = root;

      // Listen for mouse moves on the main canvas
      var mousemoveFunction = function (e) {
        // Figure out where the mouse click occurred.
        var mouseX = e.offsetX; // e.layerX;
        var mouseY = e.offsetY; // e.layerY;

        // Get the corresponding pixel color on the hidden canvas and look up the node in our map.
        // This will return that pixel's color
        var col = hiddenContext.getImageData(mouseX, mouseY, 1, 1).data;
        // Our map uses these rgb strings as keys to nodes.
        var colString = 'rgb(' + col[0] + ',' + col[1] + ',' + col[2] + ')';
        var node = colToCircle[colString];

        // Only change the popover if the user mouses over something new
        if (node !== nodeOld) {
          // Remove all previous popovers
          $('.popoverWrapper').remove();
          $('.popover').each(function () {
            $('.popover').remove();
          });
          // Only continue when the user mouses over an actual node
          if (node) {
            // Only show a popover for the leaf nodes
            if (typeof node.ID !== 'undefined') {
              // Needed for placement
              var nodeX = ((node.x - zoomInfo.centerX) * zoomInfo.scale) + centerX - 485;
              var nodeY = ((node.y - zoomInfo.centerY) * zoomInfo.scale) + centerY + 154;
              var nodeR = node.r * zoomInfo.scale;

              // Create the wrapper div for the popover
              var div = document.createElement('div');
              div.setAttribute('class', 'popoverWrapper');
              document.getElementById('chart').appendChild(div);

              // Position the wrapper right above the circle
              $('.popoverWrapper').css({
                'position': 'absolute',
                'top': nodeY - nodeR,
                'left': nodeX + padding * 5 / 4
              });

              // Show the tooltip
              $('.popoverWrapper').popover({
                placement: 'auto',
                container: 'body',
                trigger: 'manual',
                html: true,
                animation: false,
                content: function () {
                  return "<span class='nodeTooltip'>" + node.name + '</span>';
                }
              });
              $('.popoverWrapper').popover('show');
            }// if -> typeof node.ID !== "undefined"
          }// if -> node
        } // if -> node !== nodeOld

        nodeOld = node;
      };// function mousemoveFunction

      $('#canvas').on('mousemove', mousemoveFunction);
    }// if !mobileSize

    /// ///////////////////////////////////////////////////////////
    /// ////////////////// Zoom Function //////////////////////////
    /// ///////////////////////////////////////////////////////////

    // Based on the generous help by Stephan Smola
    // http://bl.ocks.org/smoli/d7e4f9199c15d71258b5
    var ease = d3version3.ease('cubic-in-out');
    var timeElapsed = 0;
    var interpolator = null;

    // Starting duration
    var duration = 1500;

    var vOld = [focus.x, focus.y, focus.r * 2.05];

    // Create the interpolation function between current view and the clicked on node
    function zoomToCanvas (focusNode) {
      // Temporarily disable click & mouseover events
      $('#canvas').css('pointer-events', 'none');

      // Remove all previous popovers - if present
      $('.popoverWrapper').remove();

      $('.popover').each(function () {
        $('.popover').remove();
      });

      // Save the ID of the clicked on node (or its parent, if it is a leaf node)
      // Only the nodes close to the currentID will have bar charts drawn
      if (focusNode === focus) currentID = '';
      else currentID = (typeof focusNode.ID === 'undefined' ? IDbyName[focusNode.name] : focusNode.ID.replace(/\.([^.]*)$/, ''));

      // Set the new focus
      focus = focusNode;
      var v = [focus.x, focus.y, focus.r * 2.05]; // The center and width of the new "viewport"

      // Create interpolation between current and new "viewport"
      interpolator = d3version3.interpolateZoom(vOld, v);

      // Set the needed "zoom" variables
      duration = Math.max(1500, interpolator.duration); // Interpolation gives back a suggested duration
      timeElapsed = 0; // Set the time elapsed for the interpolateZoom function to 0
      showText = false; // Don't show text during the zoom
      vOld = v; // Save the "viewport" of the next state as the next "old" state

      // Start animation
      stopTimer = false;
      animate();
    }// function zoomToCanvas

    // Perform the interpolation and continuously change the zoomInfo while the "transition" occurs
    function interpolateZoom (dt) {
      if (interpolator) {
        timeElapsed += dt;
        var t = ease(timeElapsed / duration); // mini interpolator that puts 0 - duration into 0 - 1 in a cubic-in-out fashion

        // Set the new zoom variables
        zoomInfo.centerX = interpolator(t)[0];
        zoomInfo.centerY = interpolator(t)[1];
        zoomInfo.scale = diameter / interpolator(t)[2];

        // After iteration is done remove the interpolater and set the fade text back into motion
        if (timeElapsed >= duration) {
          interpolator = null;
          showText = true;
          fadeText = true;
          timeElapsed = 0;

          // Draw the hidden canvas again, now that everything is settled in
          // to make sure it is in the same state as the visible canvas
          // This way the tooltip and click work correctly
          drawCanvas(hiddenContext, true);
        }// if -> timeElapsed >= duration
      }// if -> interpolator
    }// function zoomToCanvas

    // Text fading variables
    var showText = true;
    // Only show the text while you're not zooming

    var textAlpha = 1;
    // After a zoom is finished fade in the text;

    var fadeText = false;

    var fadeTextDuration = 750;
    // Function that fades in the text - Otherwise the text will be jittery during the zooming
    function interpolateFadeText (dt) {
      if (fadeText) {
        timeElapsed += dt;
        textAlpha = ease(timeElapsed / fadeTextDuration);
        if (timeElapsed >= fadeTextDuration) {
          // Enable click & mouseover events again
          $('#canvas').css('pointer-events', 'auto');

          fadeText = false; // Jump from loop after fade in is done
          stopTimer = true; // After the fade is done, stop with the redraws / animation
        }// if
      }// if
    }// function interpolateFadeText

    /// ///////////////////////////////////////////////////////////
    /// ///////////////// Other Functions /////////////////////////
    /// ///////////////////////////////////////////////////////////

    // The start angle in degrees for each of the non-node leaf titles
    var rotationText = [-14, 4, 23, -18, -10.5, -20, 20, 20, 46, -30, -25, -20, 20, 15, -30, -15, -45, 12, -15, -16, 15, 15, 5, 18, 5, 15, 20, -20, -25]; // The rotation of each arc text

    // Adjusted from: http://blog.graphicsgen.com/2015/03/html5-canvas-rounded-text.html
    function drawCircularText (ctx, text, fontSize, titleFont, centerX, centerY, radius, startAngle, kerning) {
      // startAngle:   In degrees, Where the text will be shown. 0 degrees if the top of the circle
      // kearning:     0 for normal gap between letters. Positive or negative number to expand/compact gap in pixels

      // Setup letters and positioning
      ctx.textBaseline = 'alphabetic';
      ctx.textAlign = 'center'; // Ensure we draw in exact center
      ctx.font = fontSize + 'px ' + titleFont;
      // ctx.fillStyle = "rgba(255,255,255," + textAlpha + ")";
      ctx.fillStyle = 'rgba(0,40,68,' + textAlpha + ')';

      startAngle = startAngle * (Math.PI / 180); // convert to radians
      text = text.split('').reverse().join(''); // Reverse letters

      // Rotate 50% of total angle for center alignment
      for (var j = 0; j < text.length; j++) {
        var charWid = ctx.measureText(text[j]).width;
        startAngle += ((charWid + (j === text.length - 1 ? 0 : kerning)) / radius) / 2;
      }// for j

      ctx.save(); // Save the default state before doing any transformations
      ctx.translate(centerX, centerY); // Move to center
      ctx.rotate(startAngle); // Rotate into final start position

      // Now for the fun bit: draw, rotate, and repeat
      for (j = 0; j < text.length; j++) {
        charWid = ctx.measureText(text[j]).width / 2; // half letter
        // Rotate half letter
        ctx.rotate(-charWid / radius);
        // Draw the character at "top" or "bottom" depending on inward or outward facing
        ctx.fillText(text[j], 0, -radius);
        // Rotate half letter
        ctx.rotate(-(charWid + kerning) / radius);
      }// for j

      ctx.restore(); // Restore to state as it was before transformations
    }// function drawCircularText

    /// ///////////////////////////////////////////////////////////
    /// //////////////////// Initiate /////////////////////////////
    /// ///////////////////////////////////////////////////////////

    // First zoom to get the circles to the right location
    zoomToCanvas(root);
    // Draw the hidden canvas at least once
    drawCanvas(hiddenContext, true);

    // Start the drawing loop. It will jump out of the loop once stopTimer becomes true
    var stopTimer = false;
    animate();

    // This function runs during changes in the visual - during a zoom
    function animate () {
      var dt = 0;
      d3version3.timer(function (elapsed) {
        interpolateZoom(elapsed - dt);
        interpolateFadeText(elapsed - dt);
        dt = elapsed;
        drawCanvas(context);

        return stopTimer;
      });
    }// function animate
  }// drawAll

  /// ///////////////////////////////////////////////////////////
  /// ///////////////// Other Functions /////////////////////////
  /// ///////////////////////////////////////////////////////////

  // If there is a scroll bar then its fine
  // But if there is no scrollbar prevent one from occuring when the user opens the search box
  // Otherwise the visual will seem to move to the left for a bit and that looks rather odd
  var noScrollBar = (!($(document).height() > $(window).height()));
  if (noScrollBar) {
    // Prevent scroll bars from forming
    document.documentElement.style.overflow = 'hidden'; // firefox, chrome
    document.body.scroll = 'no'; // ie only
  }// if

  // Generates the next color in the sequence, going from 0,0,0 to 255,255,255.
  // From: https://bocoup.com/weblog/2d-picking-in-canvas
  var nextCol = 1;

  function genColor () {
    var ret = [];
    // via http://stackoverflow.com/a/15804183
    if (nextCol < 16777215) {
      ret.push(nextCol & 0xff); // R
      ret.push((nextCol & 0xff00) >> 8); // G
      ret.push((nextCol & 0xff0000) >> 16); // B

      nextCol += 100; // This is exagerated for this example and would ordinarily be 1.
    }
    var col = 'rgb(' + ret.join(',') + ')';
    return col;
  }// function genColor

  // From http://stackoverflow.com/questions/2936112/text-wrap-in-a-canvas-element
  function getLines (ctx, text, maxWidth, fontSize, titleFont) {
    var words = text.split(' ');
    var lines = [];
    var currentLine = words[0];

    for (var i = 1; i < words.length; i++) {
      var word = words[i];
      ctx.font = fontSize + 'px ' + titleFont;
      var width = ctx.measureText(currentLine + ' ' + word).width;
      if (width < maxWidth) {
        currentLine += ' ' + word;
      } else {
        lines.push(currentLine);
        currentLine = word;
      }
    }
    lines.push(currentLine);
    return lines;
  }// function getLines
});
