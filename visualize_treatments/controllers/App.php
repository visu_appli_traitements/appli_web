<?php

/**
 * Copyright:
 * Guillaume Bernard <contact@guillaume-bernard.fr>
 * Thomas Duveau <tduveau@etudiant.univ-lr.fr>
 * Loïc Favrelière <lfavreli@etudiant.univ-lr.fr>
 * Nicola Foissac <nfoissac@etudiant.univ-lr.fr>
 *
 * This software is a computer program whose purpose is to visualize in
 * a simple way the record of procesing activites as defined in the GDPR.
 *
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 **/

defined('BASEPATH') or exit('No direct script access allowed');

class App extends CI_Controller
{

    /**
     * Index Page for this App controller.
     * Maps to the following URL
     *         {BASE_URL}/
     *    - or -
     *         {BASE_URL}/app
     *    - or -
     *         {BASE_URL}/app/index
     */
    public function index()
    {
        $this->home('simple');
    }
    
    public function home($view = 'simple')
    {
        // Set the page title
        $this->template->set('title', 'Accueil - Visualisation des traitements');
        
        // Initiate view variable according to the page rendered
        $view = ($view != 'simple' && $view != 'classic') ? 'simple' : $view;
        $data['view'] = $view;
        $this->template->set('view', $view);
        
        // Load treatments
        $data['dataset'] = file_get_contents('assets/data/data.json');
        
        // Loading the Home views in the default template
        $this->template->load('default_layout', 'contents', 'components/home', $data);
    }

    public function credits()
    {
        $this->template->set('title', 'Contact & Mentions légales');
        $this->template->load('default_layout', 'contents', 'components/credits');
    }

    public function methodologie()
    {
        $this->template->set('title', 'Méthodologie de travail');
        $this->template->load('default_layout', 'contents', 'components/methodologie');
    }
}
