<!DOCTYPE html>
<html lang="fr">

    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <title><?php echo $title; ?></title>

        <!-- Bootstrap -->
        <link rel="stylesheet" href="//stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

        <!-- Datatable -->
        <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/c3/0.6.12/c3.min.css">
        <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">

        <!-- Personnal Style -->
        <link rel="stylesheet" type="text/css" href="/assets/css/style.css">

        <!-- Favicon -->
        <!-- Generated on: https://favicon.io/favicon-generator/ & Created by: John Sorrentino -->
        <link rel="apple-touch-icon" sizes="180x180" href="/assets/icon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/assets/icon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/assets/icon/favicon-16x16.png">
        <link rel="manifest" href="/assets/icon/site.webmanifest">
    </head>

    <body>
        
        <!-- NAVBAR -->
        <header>
            <nav class="navbar navbar-expand-md navbar-light fixed-top shadow">
                <div class="navbar-header">
                    <a class="navbar-brand" href="/">
                          <svg id="navbar-icon" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="d-block mx-auto">
                            <circle cx="12" cy="12" r="10"></circle>
                            <line x1="14.31" y1="8" x2="20.05" y2="17.94"></line>
                            <line x1="9.69" y1="8" x2="21.17" y2="8"></line>
                            <line x1="7.38" y1="12" x2="13.12" y2="2.06"></line>
                            <line x1="9.69" y1="16" x2="3.95" y2="6.06"></line>
                            <line x1="14.31" y1="16" x2="2.83" y2="16"></line>
                            <line x1="16.62" y1="12" x2="10.88" y2="21.94"></line>
                        </svg>
                      </a>
                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="/">Accueil</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/app/methodologie">Méthodologie</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/app/credits">Contact</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-toggle="modal" data-target="#help">Aide</a>
                        </li>
                    </ul>

                    <!-- Section de changement de vue -->
                    <ul class="nav navbar-nav navbar-right">
                        <span class="pt-2">Accéder à la vue :</span>
                        <li><a class="nav-link" href="/app/home/simple"><span class="glyphicon glyphicon-user"></span>Simplifiée</a></li>
                        <li><a class="nav-link" href="/app/home/classic"><span class="glyphicon glyphicon-log-in"></span>Classique</a></li>
                    </ul>
                </div>
            </nav>
        </header>
    
        <!-- Help Modal -->
        <div class="modal fade" id="help" tabindex="-1" role="dialog" aria-labelledby="changeProfileTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Guide utilisateur</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                    </div>
                    <div class="modal-body">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris non tellus at enim sodales ultricies ut nec est. Vestibulum tincidunt, justo in tincidunt auctor, mauris erat tempus leo, sit amet porta augue sem in felis. Donec id elit egestas, volutpat purus eget, fermentum sem. Maecenas iaculis quis sem id convallis. Sed sed ante fermentum enim gravida sagittis non sit amet mi. Ut a gravida magna. Sed imperdiet finibus efficitur. Praesent ligula nulla, vulputate venenatis eleifend quis, vestibulum et tellus. Phasellus a risus elit. Vestibulum vulputate lacus et nibh gravida sagittis. Duis non dolor sapien. </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
    
        <div class="container-fluid">
            <div class="row">
                <?php echo $contents;?>
            </div>
        </div>
        
        <footer class="my-3 pt-3 text-muted text-center text-small">
            <p class="mb-1">Made with ❤ at La Rochelle Université · Janvier 2019</p>
            <ul class="list-inline">
                <li class="list-inline-item"><a href="/app/credits">Contact</a></li>
                <li class="list-inline-item">·</a>
                </li>
                <li class="list-inline-item"><a href="/app/credits">Mentions légales</a></li>
            </ul>
        </footer>
        
        <!-- jQuery -->
        <script src="//code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    
        <!-- Bootstrap -->
        <script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
        <script src="//stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    
        <!-- D3.js & C3.js -->
        <script src="//cdnjs.cloudflare.com/ajax/libs/d3/3.5.6/d3.min.js" type="text/javascript" charset="utf-8"></script>
        <script src="//d3js.org/queue.v1.min.js"></script>
        <script>
            var d3version3 = d3;
                window.d3 = null;
        </script>
    
        <script src="//d3js.org/d3.v5.min.js" type="text/javascript" charset="utf-8"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/c3/0.6.12/c3.min.js" type="text/javascript" charset="utf-8"></script>
    
        <!-- Datatables -->
        <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" type="text/javascript" charset="utf8"></script>
        <script src="//cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js" type="text/javascript" charset="utf8"></script>
    
        <!-- Personal JavaScript -->
        <script src="/assets/js/main.js" type="text/javascript"></script>
    
        <!-- Script canvas -->
        <script src="/assets/js/canvas_dataviz.js" type="text/javascript"></script>
    </body>

</html>