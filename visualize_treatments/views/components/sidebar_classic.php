<?php

/**
 * Copyright:
 * Guillaume Bernard <contact@guillaume-bernard.fr>
 * Thomas Duveau <tduveau@etudiant.univ-lr.fr>
 * Loïc Favrelière <lfavreli@etudiant.univ-lr.fr>
 * Nicola Foissac <nfoissac@etudiant.univ-lr.fr>
 *
 * This software is a computer program whose purpose is to visualize in
 * a simple way the record of procesing activites as defined in the GDPR.
 *
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 **/
?>

<nav class="col-md-2 d-none d-md-block bg-light sidebar">
    <div class="sidebar-sticky">
        <ul class="nav flex-column ml-2">
            <li class="title">
                <h3>Filtres</h3>
            </li>
            
            <!-- Section - Filter treatments by source -->
            <h4 class="sidebar-heading ml-4 text-muted">
              <span>Traitements par service</span>
            </h4>
            <li class="sidebar-custom-radio mt-2 ml-4">
                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                    <label class="btn btn-outline-info btn-sm source active" data-toggle="tooltip"
                           data-placement="bottom" title="Destinataire des traitements">
                        <input type="radio" name="source" id="destinataire" value="destinataire"
                               autocomplete="off" checked>Destinataire
                    </label>
                    <label class="btn btn-outline-info btn-sm source" data-toggle="tooltip" data-placement="bottom"
                           title="Service mettant en oeuvre le traitement">
                        <input type="radio" name="source" id="source_mise_oeuvres" value="source_mise_oeuvres"
                               autocomplete="off">Exploitant
                    </label>
                </div>
            </li>

            <!-- Section - Filter treatments by categories -->
            <h5 class="sidebar-heading mt-4 ml-4 text-muted">
              <span>Traitements par catégorie</span>
            </h5>
            <li class="sidebar-custom-checkbox ml-2">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                     stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                    <path d="M16 4h2a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h2"></path>
                    <rect x="8" y="2" width="8" height="4" rx="1" ry="1"></rect>
                </svg>
                <label class="form-check-label ml-2">
                    Administratif
                </label>
                <input class="form-check-input" name="sidebarClassic" type="checkbox" value="Administratif">
            </li>
            <li class="sidebar-custom-checkbox ml-2">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                     stroke="currentColor" stroke-width="2" stroke-linecap="round"
                     stroke-linejoin="round">
                    <path d="M12 22s8-4 8-10V5l-8-3-8 3v7c0 6 8 10 8 10z"></path>
                </svg>

                <label class="form-check-label ml-2">
                    Judiciaire
                </label>
                <input class="form-check-input" name="sidebarClassic" type="checkbox" value="Judiciaire">

            </li>
            <li class="sidebar-custom-checkbox ml-2">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                     stroke-linejoin="round" class="feather feather-dollar-sign">
                    <line x1="12" y1="1" x2="12" y2="23"></line>
                    <path d="M17 5H9.5a3.5 3.5 0 0 0 0 7h5a3.5 3.5 0 0 1 0 7H6"></path>
                </svg>
                <label class="form-check-label ml-2">
                    Finance
                </label>
                <input class="form-check-input" name="sidebarClassic" type="checkbox" value="Finance">

            </li>
            <li class="sidebar-custom-checkbox ml-2">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                     stroke="currentColor" stroke-width="2" stroke-linecap="round"
                     stroke-linejoin="round">
                    <path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path>
                </svg>
                <label class="form-check-label ml-2">
                    Santé
                </label>
                <input class="form-check-input" name="sidebarClassic" type="checkbox" value="Santé">

            </li>
            <li class="sidebar-custom-checkbox ml-2">
                <svg xmlns="http://www.w3.org/2000/svg"
                     width="24" height="24" viewBox="0 0 24 24" fill="none"
                     stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                    <rect x="3" y="11" width="18" height="11" rx="2" ry="2"></rect>
                    <path d="M7 11V7a5 5 0 0 1 10 0v4"></path>
                </svg>
                <label class="form-check-label ml-2">
                    Sécurité
                </label>
                <input class="form-check-input" name="sidebarClassic" type="checkbox" value="Sécurité">

            </li>
            <li class="sidebar-custom-checkbox ml-2">
                <svg viewBox="0 0 24 24" width="24" height="24" xmlns="http://www.w3.org/2000/svg"
                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                     stroke-linejoin="round">
                    <path d="M18 8h1a4 4 0 0 1 0 8h-1"></path>
                    <path d="M2 8h16v9a4 4 0 0 1-4 4H6a4 4 0 0 1-4-4V8z"></path>
                    <line x1="6" y1="1" x2="6" y2="4"></line>
                    <line x1="10" y1="1" x2="10" y2="4"></line>
                    <line x1="14" y1="1" x2="14" y2="4"></line>
                </svg>

                <label class="form-check-label ml-2">
                    Services
                </label>
                <input class="form-check-input" name="sidebarClassic" type="checkbox" value="Services">

            </li>
            <li class="sidebar-custom-checkbox ml-2">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                     stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                    <rect x="1" y="3" width="15" height="13"></rect>
                    <polygon points="16 8 20 8 23 11 23 16 16 16 16 8"></polygon>
                    <circle cx="5.5" cy="18.5" r="2.5"></circle>
                    <circle cx="18.5" cy="18.5" r="2.5"></circle>
                </svg>

                <label class="form-check-label ml-2">
                    Urbanisme
                </label>
                <input class="form-check-input" name="sidebarClassic" type="checkbox" value="Urbanisme">

            </li>
            <li class="sidebar-custom-checkbox ml-2">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                     stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                    <path d="M10 13a5 5 0 0 0 7.54.54l3-3a5 5 0 0 0-7.07-7.07l-1.72 1.71"></path>
                    <path d="M14 11a5 5 0 0 0-7.54-.54l-3 3a5 5 0 0 0 7.07 7.07l1.71-1.71"></path>
                </svg>
                <label class="form-check-label ml-2">
                    Associatif
                </label>
                <input class="form-check-input" name="sidebarClassic" type="checkbox" value="Associatif">

            </li>
            <li class="sidebar-custom-checkbox ml-2">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                     stroke-linejoin="round" class="feather feather-layers">
                    <polygon points="12 2 2 7 12 12 22 7 12 2"></polygon>
                    <polyline points="2 17 12 22 22 17"></polyline>
                    <polyline points="2 12 12 17 22 12"></polyline>
                </svg>
                <label class="form-check-label ml-2">
                    Autres
                </label>
                <input class="form-check-input" name="sidebarClassic" type="checkbox" value="Autres">

            </li>
            <li class="mt-3 ml-4 pb-4">
                <button type="button" id="reset" class="btn btn-dark btn-sm">Réinitialiser</button>
            </li>
        </ul>
    </div>
</nav>
