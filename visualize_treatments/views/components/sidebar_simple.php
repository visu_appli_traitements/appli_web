<?php

/**
 * Copyright:
 * Guillaume Bernard <contact@guillaume-bernard.fr>
 * Thomas Duveau <tduveau@etudiant.univ-lr.fr>
 * Loïc Favrelière <lfavreli@etudiant.univ-lr.fr>
 * Nicola Foissac <nfoissac@etudiant.univ-lr.fr>
 *
 * This software is a computer program whose purpose is to visualize in
 * a simple way the record of procesing activites as defined in the GDPR.
 *
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 **/
?>

<nav class="col-md-3 d-none d-md-block bg-light sidebar">
    <div class="sidebar-sticky">
        <ul class="nav flex-column">
            <li class="title">
                <h3>Mode d'emploi</h3>
            </li>
            <li>
                <p class="mx-3">Dans cette visualisation, vous pouvez examiner comment les traitements associés à vos <b>données personnelles</b>, référencer par le registre légal défini par la 
                    <a title="Le registre des activités de traitement" target="_blank" href="https://www.cnil.fr/fr/RGDP-le-registre-des-activites-de-traitement">CNIL</a> et le 
                    <a title="RGPD - Responsable du traitement et sous-traitant" target="_blank" href="https://www.cnil.fr/fr/reglement-europeen-protection-donnees/chapitre4#Article30">RGPD</a>, sont répartis.
                </p>
                <p class="mx-3">
                    Chaque cercle de couleur bleu englobe les traitements qui relèvent de sa catégorie. Les cercles blancs représentent les traitements et la finalité qu'ils servent. 
                    Vous pouvez cliquer sur un cercle pour zoomer. Pour revenir à l'état initial, cliquer sur le cercle bleu clair.
                </p>
                <p class="mx-3">Les traitements sont regroupés en catégorie et sous-catégories : <b>associatif</b>, <b>sécurité</b>, <b>santé</b>...</p>
                <p class="mx-3">Les données sont issues de la 
                    <a title="Registre légal CNIL - Mairie de Toulouse" target="_blank" href="https://data.toulouse-metropole.fr/explore/dataset/registre-legal-cnil-ville-de-toulouse/information/">ville</a>
                        et l'<a title="Registre légal CNIL - Mairie de Toulouse" target="_blank" href="https://data.toulouse-metropole.fr/explore/dataset/registre-legal-cnil-toulouse-metropole/information/">agglomération</a>
                    de Toulouse.
                </p>
                <p class="mx-3 pb-3">Si vous souhaitez obtenir une vue plus détaillée, accédez à la vue 
                <a title="Vue classique" href="/app/home/classic">classique</a>.</p>
            </li>
        </ul>
    </div>
</nav>
