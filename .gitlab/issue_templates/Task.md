⚠⚠ Please read this! ⚠⚠

Before opening a new issue, make sure to search for keywords in the issues filtered by the "regression" or "bug" labels, and verify the issue you're about to submit isn't a duplicate. Please remove this notice if you're confident your issue isn't a duplicate.

A task is a specific, sequenced set of operations, like “”. Thoses steps are ordered and give all developers a clear view on the advancement. You can used a checkable list in order to explain your steps. A good task as an estimate time of maximum 2 hours.

---

### Summary

A summary telling what is excpeted for this task and the estimated steps to achieve them.

[ ] First step

[ ] Seconde step

### Test suite

A list of tests that validate the development tasks.

### Note

/estimate 2h

/label ~task
