# Installation instructions

This project is based on [CodeIgniter](https://codeigniter.com/) and is designed to run on PHP 7.3. The framework itself is not part of the repository. In order to install it, move to the project folder and run:

```bash
mkdir vendor/codeIgniter -p
curl -L https://github.com/bcit-ci/CodeIgniter/archive/3.1.9.tar.gz|tar xz --strip-components=1 -C vendor/codeIgniter
chmod o+rx -R vendor/codeIgniter
```

Otherwise, you can simply execute the **install.sh** script, using:
```bash
./install.sh
```

Once done, the easiest way to launch the application is to use Docker. Please ensure Docker and `docker-compose` are installed on your system. For instance, on a RHEL/Centos/Fedora operating system, do:

```bash
yum install docker docker-compose -y
```

Then, you can start the project using `docker-compose` (see [Docker compose documentation](https://docs.docker.com/compose/)):

```bash
docker-compose up
```

The web server is running on [http://localhost:8080](http://localhost:8080).

## Possible issues

### Permission error

For development purposes, it is recommended (but not in production, you’re supposed to know what you do 🙃) to disable SeLinux if enabled on your system:

```bash
setenforce 0
```

Ensure that the project permissions are properly defined to **655**.

# License

This project is licensed under [CECiLL-B](http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.txt) software license. You’ll find a copy in this repository. This software is brought to you by:

* Guillaume Bernard <etudiant@guillaume-bernard.fr>

* Thomas Duveau <tduveau@etudiant.univ-lr.fr>

* Loïc Favrelière <lfavreli@etudiant.univ-lr.fr>

* Nicola Foissac <nfoissac@etudiant.univ-lr.fr>

Based on an original idea by:

* Virgnie Steiner <virginie.steiner@ville-larochelle.fr>
