#!/bin/bash

url=https://code.guillaume-bernard.fr/guilieb/libShell/archive/master.tar.gz; libShell_dir="$(mktemp -d)" && curl -s "${url}" | tar xz --strip 1 -C "${libShell_dir}" && source "${libShell_dir}"/libShell.sh

enableLogger ${ERROR} ${STDOUT}
Log ${NOTE} "Installing project dependencies…"

codeIgniterULR="https://github.com/bcit-ci/CodeIgniter/archive/3.1.9.tar.gz"
codeIgniter="vendor/codeIgniter"

Log ${INFO} "Creating base directories…"
[[ ! -d "${codeIgniter}" ]] && mkdir "${codeIgniter}" -p

Log ${INFO} "Dowloading dependencies…"
[[ ! -d "${codeIgniter}/system" ]] && curl -Ls "${codeIgniterULR}"|tar xz --strip-components=1 -C "${codeIgniter}"
chmod o+rx -R "${codeIgniter}"

Log ${INFO} "Installing NodeJS dependencies"
npm install

Log ${WARNING} "If running on Docker, please, ensure the correct permissions are set."
